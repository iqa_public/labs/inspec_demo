# inspec_demo

The localtests folder was created by [inspec](https://www.inspec.io/) using this command line:

    $ inspec init profile localtests

The `controls` folder and `inspec.yml` were edited to customize.

# Running locally

You can build the container in which it runs, and run the tests, in a single command:

    $ docker-compose run inspec_demo

# Running on GitLab

The `.gitlab-ci.yml` does the same thing as the combined `docker-compose.yml` and `test.sh` files. It builds the Docker image then uses it to run the tests.